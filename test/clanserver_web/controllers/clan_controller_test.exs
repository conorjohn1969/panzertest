defmodule ClanserverWeb.ClanControllerTest do
  use ClanserverWeb.ConnCase

  alias Clanserver.Accounts

  @create_attrs %{clantag: "some clantag", name: "some name"}
  @update_attrs %{clantag: "some updated clantag", name: "some updated name"}
  @invalid_attrs %{clantag: nil, name: nil}

  def fixture(:clan) do
    {:ok, clan} = Accounts.create_clan(@create_attrs)
    clan
  end

  describe "index" do
    test "lists all clans", %{conn: conn} do
      conn = get(conn, Routes.clan_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Clans"
    end
  end

  describe "new clan" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.clan_path(conn, :new))
      assert html_response(conn, 200) =~ "New Clan"
    end
  end

  describe "create clan" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.clan_path(conn, :create), clan: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.clan_path(conn, :show, id)

      conn = get(conn, Routes.clan_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Clan"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.clan_path(conn, :create), clan: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Clan"
    end
  end

  describe "edit clan" do
    setup [:create_clan]

    test "renders form for editing chosen clan", %{conn: conn, clan: clan} do
      conn = get(conn, Routes.clan_path(conn, :edit, clan))
      assert html_response(conn, 200) =~ "Edit Clan"
    end
  end

  describe "update clan" do
    setup [:create_clan]

    test "redirects when data is valid", %{conn: conn, clan: clan} do
      conn = put(conn, Routes.clan_path(conn, :update, clan), clan: @update_attrs)
      assert redirected_to(conn) == Routes.clan_path(conn, :show, clan)

      conn = get(conn, Routes.clan_path(conn, :show, clan))
      assert html_response(conn, 200) =~ "some updated clantag"
    end

    test "renders errors when data is invalid", %{conn: conn, clan: clan} do
      conn = put(conn, Routes.clan_path(conn, :update, clan), clan: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Clan"
    end
  end

  describe "delete clan" do
    setup [:create_clan]

    test "deletes chosen clan", %{conn: conn, clan: clan} do
      conn = delete(conn, Routes.clan_path(conn, :delete, clan))
      assert redirected_to(conn) == Routes.clan_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.clan_path(conn, :show, clan))
      end
    end
  end

  defp create_clan(_) do
    clan = fixture(:clan)
    {:ok, clan: clan}
  end
end
