defmodule Clanserver.AccountsTest do
  use Clanserver.DataCase

  alias Clanserver.Accounts

  describe "users" do
    alias Clanserver.Accounts.User

    @valid_attrs %{nickname: "some nickname"}
    @update_attrs %{nickname: "some updated nickname"}
    @invalid_attrs %{nickname: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.nickname == "some nickname"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
      assert user.nickname == "some updated nickname"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end

  describe "clans" do
    alias Clanserver.Accounts.Clan

    @valid_attrs %{clantag: "some clantag", name: "some name"}
    @update_attrs %{clantag: "some updated clantag", name: "some updated name"}
    @invalid_attrs %{clantag: nil, name: nil}

    def clan_fixture(attrs \\ %{}) do
      {:ok, clan} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_clan()

      clan
    end

    test "list_clans/0 returns all clans" do
      clan = clan_fixture()
      assert Accounts.list_clans() == [clan]
    end

    test "get_clan!/1 returns the clan with given id" do
      clan = clan_fixture()
      assert Accounts.get_clan!(clan.id) == clan
    end

    test "create_clan/1 with valid data creates a clan" do
      assert {:ok, %Clan{} = clan} = Accounts.create_clan(@valid_attrs)
      assert clan.clantag == "some clantag"
      assert clan.name == "some name"
    end

    test "create_clan/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_clan(@invalid_attrs)
    end

    test "update_clan/2 with valid data updates the clan" do
      clan = clan_fixture()
      assert {:ok, %Clan{} = clan} = Accounts.update_clan(clan, @update_attrs)
      assert clan.clantag == "some updated clantag"
      assert clan.name == "some updated name"
    end

    test "update_clan/2 with invalid data returns error changeset" do
      clan = clan_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_clan(clan, @invalid_attrs)
      assert clan == Accounts.get_clan!(clan.id)
    end

    test "delete_clan/1 deletes the clan" do
      clan = clan_fixture()
      assert {:ok, %Clan{}} = Accounts.delete_clan(clan)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_clan!(clan.id) end
    end

    test "change_clan/1 returns a clan changeset" do
      clan = clan_fixture()
      assert %Ecto.Changeset{} = Accounts.change_clan(clan)
    end
  end

  describe "invites" do
    alias Clanserver.Accounts.Invite

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def invite_fixture(attrs \\ %{}) do
      {:ok, invite} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_invite()

      invite
    end

    test "list_invites/0 returns all invites" do
      invite = invite_fixture()
      assert Accounts.list_invites() == [invite]
    end

    test "get_invite!/1 returns the invite with given id" do
      invite = invite_fixture()
      assert Accounts.get_invite!(invite.id) == invite
    end

    test "create_invite/1 with valid data creates a invite" do
      assert {:ok, %Invite{} = invite} = Accounts.create_invite(@valid_attrs)
    end

    test "create_invite/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_invite(@invalid_attrs)
    end

    test "update_invite/2 with valid data updates the invite" do
      invite = invite_fixture()
      assert {:ok, %Invite{} = invite} = Accounts.update_invite(invite, @update_attrs)
    end

    test "update_invite/2 with invalid data returns error changeset" do
      invite = invite_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_invite(invite, @invalid_attrs)
      assert invite == Accounts.get_invite!(invite.id)
    end

    test "delete_invite/1 deletes the invite" do
      invite = invite_fixture()
      assert {:ok, %Invite{}} = Accounts.delete_invite(invite)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_invite!(invite.id) end
    end

    test "change_invite/1 returns a invite changeset" do
      invite = invite_fixture()
      assert %Ecto.Changeset{} = Accounts.change_invite(invite)
    end
  end
end
