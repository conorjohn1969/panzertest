defmodule ClanserverWeb.InviteController do
  use ClanserverWeb, :controller

  alias Clanserver.Accounts
  alias Clanserver.Accounts.Invite

  def index(conn, _params) do
    invites = Accounts.list_invites()
    render(conn, "index.html", invites: invites)
  end

  def new(conn, _params) do
    changeset = Accounts.change_invite(%Invite{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"invite" => invite_params}) do
    case Accounts.create_invite(invite_params) do
      {:ok, invite} ->
        conn
        |> put_flash(:info, "Invite created successfully.")
        |> redirect(to: Routes.invite_path(conn, :show, invite))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    invite = Accounts.get_invite!(id)
    render(conn, "show.html", invite: invite)
  end

  def edit(conn, %{"id" => id}) do
    invite = Accounts.get_invite!(id)
    changeset = Accounts.change_invite(invite)
    render(conn, "edit.html", invite: invite, changeset: changeset)
  end

  def update(conn, %{"id" => id, "invite" => invite_params}) do
    invite = Accounts.get_invite!(id)

    case Accounts.update_invite(invite, invite_params) do
      {:ok, invite} ->
        conn
        |> put_flash(:info, "Invite updated successfully.")
        |> redirect(to: Routes.invite_path(conn, :show, invite))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", invite: invite, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    invite = Accounts.get_invite!(id)
    {:ok, _invite} = Accounts.delete_invite(invite)

    conn
    |> put_flash(:info, "Invite deleted successfully.")
    |> redirect(to: Routes.invite_path(conn, :my_invites))
  end

  def invite_user(conn, %{"id" => id}) do
    target_user = Accounts.get_user!(id)
    clan = Accounts.get_clan_by(leader_id: conn.assigns.current_user.id)

    case clan do
       nil ->
         conn
         |> put_flash(:info, "Create clan first.")
         |> redirect(to: Routes.clan_path(conn, :new))
       clan ->
         invite_params =
           %{}
           |> Map.put(:clan_id, clan.id)
           |> Map.put(:user_id, target_user.id)

         Accounts.create_invite(invite_params)

         conn
         |> put_flash(:info, "User invited successfully.")
         |> redirect(to: Routes.user_path(conn, :listing))
    end
  end

  def my_invites(conn, _params) do
    invites = Accounts.get_invites_by_user(conn.assigns.current_user.id)

    render(conn, "my_invites.html", invites: invites)
  end

  def accept_invite(conn, %{"id" => id}) do
    invite = Accounts.get_invite!(id)
    user = Accounts.get_user!(conn.assigns.current_user.id)
    user_params = %{} |> Map.put(:clan_id, invite.clan_id)
    Accounts.add_user_to_clan(user, user_params)
    Accounts.delete_invite(invite)

    conn
    |> put_flash(:info, "Invite accepted successfully.")
    |> redirect(to: Routes.invite_path(conn, :my_invites))
  end

  def remove_user_from_clan(conn, %{"id" => id}) do
    target_user = Accounts.get_user!(id)
    Accounts.remove_user_from_clan(target_user)

    conn
    |> put_flash(:info, "User sussesfully removed from clan.")
    |> redirect(to: Routes.clan_path(conn, :clan_list))
  end
end
