defmodule ClanserverWeb.ClanController do
  use ClanserverWeb, :controller

  alias Clanserver.Accounts
  alias Clanserver.Accounts.Clan

  def index(conn, _params) do
    clans = Accounts.list_clans()
    render(conn, "index.html", clans: clans)
  end

  def listing(conn, _params) do
    clans = Accounts.list_clans()
    render(conn, "listing.html", clans: clans)
  end

  def new(conn, _params) do
    clan = Accounts.get_clan_by(leader_id: conn.assigns.current_user.id)

    case clan do
      nil ->
        changeset = Accounts.change_clan(%Clan{})
        render(conn, "new_my_clan.html", changeset: changeset)

      clan ->
        conn
        |> redirect(to: Routes.clan_path(conn, :edit, clan))
    end
  end

  def create(conn, %{"clan" => clan_params}) do
    clan_params = Map.put(clan_params, "leader_id", conn.assigns.current_user.id)

    case Accounts.create_clan(clan_params) do
      {:ok, clan} ->
        conn
        |> put_flash(:info, "Clan created successfully.")
        |> redirect(to: Routes.clan_path(conn, :listing))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    clan = Accounts.get_clan!(id)
    render(conn, "show.html", clan: clan)
  end

  def edit(conn, %{"id" => _id}) do
    clan = Accounts.get_clan_by(leader_id: conn.assigns.current_user.id)
    changeset = Accounts.change_clan(clan)
    render(conn, "edit_my_clan.html", clan: clan, changeset: changeset)
  end

  def update(conn, %{"id" => id, "clan" => clan_params}) do
    clan = Accounts.get_clan!(id)

    case Accounts.update_clan(clan, clan_params) do
      {:ok, clan} ->
        conn
        |> put_flash(:info, "Clan updated successfully.")
        |> redirect(to: Routes.clan_path(conn, :listing))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", clan: clan, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    clan = Accounts.get_clan!(id)
    {:ok, _clan} = Accounts.delete_clan(clan)

    conn
    |> put_flash(:info, "Clan deleted successfully.")
    |> redirect(to: Routes.clan_path(conn, :index))
  end

  def clan_list(conn, _params) do
    clan = Accounts.get_clan_by_leader(conn.assigns.current_user.id)

    case clan do
       nil ->
         conn
         |> put_flash(:info, "Create clan first.")
         |> redirect(to: Routes.clan_path(conn, :new))
       clan ->
         users = Accounts.get_users_by_clan(clan.id)
         render(conn, "clanlist.html", users: users)
    end
  end
end
