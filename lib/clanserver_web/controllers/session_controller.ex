defmodule ClanserverWeb.SessionController do
  use ClanserverWeb, :controller

  alias ClanserverWeb.Authentication

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => %{"login" => login, "password" => password}}) do
    case Authentication.login_by_login_and_pass(conn, login, password) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, gettext("Welcome %{name}", name: conn.assigns.current_user.nickname))
        |> redirect(to: Routes.clan_path(conn, :listing))

      {:error, _reason, conn} ->
        conn
        |> put_flash(:error, gettext("Incorrect credentials"))
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> Authentication.logout()
    |> put_flash(:info, gettext("Logout successful"))
    |> redirect(to: Routes.session_path(conn, :new))
  end
end
