defmodule ClanserverWeb.Authorization do
  import Plug.Conn
  alias ClanserverWeb.Router.Helpers, as: Routes

  def init(opts), do: opts

  def call(conn, _opts) do
    case conn.assigns.current_user do
      nil ->
        conn
        |> Phoenix.Controller.put_flash(:error, "Please log in")
        |> Phoenix.Controller.redirect(to: Routes.session_path(conn, :new))
        |> halt()

      _ ->
        conn
    end
  end
end
