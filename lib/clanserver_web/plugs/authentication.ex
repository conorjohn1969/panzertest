defmodule ClanserverWeb.Authentication do
  import Plug.Conn

  alias Clanserver.Accounts

  def init(opts), do: opts

  def call(conn, _opts) do
    token = get_session(conn, :token)
    user = token && Accounts.get_user_by(token: token)

    conn
    |> assign(:current_user, user)
  end

  def login(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:token, user.token)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end

  def login_by_login_and_pass(conn, login, password) do
    user = Accounts.get_user_by_login(login)

    cond do
      user && Bcrypt.verify_pass(password, user.password_hash) ->
        {:ok, login(conn, user)}

      user ->
        {:error, :unauthorized, conn}

      true ->
        Bcrypt.no_user_verify()
        {:error, :not_found, conn}
    end
  end

  def random_token do
    :crypto.strong_rand_bytes(64) |> Base.url_encode64()
  end
end
