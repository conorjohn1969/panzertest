defmodule ClanserverWeb.Router do
  use ClanserverWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authentication do
    plug ClanserverWeb.Authentication
  end

  pipeline :authorized do
    plug ClanserverWeb.Authentication
    plug ClanserverWeb.Authorization
  end

  scope "/", ClanserverWeb do
    pipe_through :browser

  end

  scope "/", ClanserverWeb do
    pipe_through [:browser, :authentication]
    resources "/session", SessionController, only: [:new, :create, :delete], singleton: true
    resources "/users", UserController, only: [:new, :create]
  end

  scope "/", ClanserverWeb do
    pipe_through [:browser, :authorized]
    get "/", PageController, :index
    resources "/clans", ClanController
    resources "/invites", InviteController
    get "/inviteuser/:id", InviteController, :invite_user
    get "/removeuser/:id", InviteController, :remove_user_from_clan
    get "/my_invites", InviteController, :my_invites
    get "/accept_invite/:id", InviteController, :accept_invite
    get "/clan_list", ClanController, :clan_list
    get "/clan_listing", ClanController, :listing
    get "/user_list", UserController, :listing
#    get "/invites/new", InviteController, :new
    resources "/users", UserController, only: [:delete, :index, :show, :edit]
  end

  # Other scopes may use custom stacks.
  # scope "/api", ClanserverWeb do
  #   pipe_through :api
  # end
end
