defmodule Clanserver.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Clanserver.Repo

  alias Clanserver.Accounts.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
    |> Repo.preload(:clan)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  def get_user_by(clauses) do
    Repo.get_by(User, clauses)
    |> Repo.preload(:clan)
  end

  def get_user_by_login(login) do
    Repo.get_by(User, login: login)
  end

  def get_users_by_clan(clan_id) do
    User
    |> where(clan_id: ^clan_id)
    |> Repo.all()
    |> Repo.preload(:clan)
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def add_user_to_clan(%User{} = user, attrs) do
    user
    |> User.clan_changeset(attrs)
    |> Repo.update()
  end

  def remove_user_from_clan(%User{} = user) do
    user
    |> User.remove_from_clan_changeset(%{})
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  alias Clanserver.Accounts.Clan

  @doc """
  Returns the list of clans.

  ## Examples

      iex> list_clans()
      [%Clan{}, ...]

  """
  def list_clans do
    Repo.all(Clan)
  end

  @doc """
  Gets a single clan.

  Raises `Ecto.NoResultsError` if the Clan does not exist.

  ## Examples

      iex> get_clan!(123)
      %Clan{}

      iex> get_clan!(456)
      ** (Ecto.NoResultsError)

  """
  def get_clan!(id), do: Repo.get!(Clan, id)

  def get_clan_by(clauses) do
    Repo.get_by(Clan, clauses)
  end

  def get_clan_by_leader(leader_id) do
    get_clan_by(leader_id: leader_id)
  end

  @doc """
  Creates a clan.

  ## Examples

      iex> create_clan(%{field: value})
      {:ok, %Clan{}}

      iex> create_clan(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_clan(attrs \\ %{}) do
    %Clan{}
    |> Clan.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a clan.

  ## Examples

      iex> update_clan(clan, %{field: new_value})
      {:ok, %Clan{}}

      iex> update_clan(clan, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_clan(%Clan{} = clan, attrs) do
    clan
    |> Clan.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Clan.

  ## Examples

      iex> delete_clan(clan)
      {:ok, %Clan{}}

      iex> delete_clan(clan)
      {:error, %Ecto.Changeset{}}

  """
  def delete_clan(%Clan{} = clan) do
    Repo.delete(clan)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking clan changes.

  ## Examples

      iex> change_clan(clan)
      %Ecto.Changeset{source: %Clan{}}

  """
  def change_clan(%Clan{} = clan) do
    Clan.changeset(clan, %{})
  end

  alias Clanserver.Accounts.Invite

  @doc """
  Returns the list of invites.

  ## Examples

      iex> list_invites()
      [%Invite{}, ...]

  """
  def list_invites do
    Repo.all(Invite)
  end

  def get_invites_by_user(user_id) do
    Invite
    |> where(user_id: ^user_id)
    |> Repo.all()
    |> Repo.preload(:clan)
  end

  @doc """
  Gets a single invite.

  Raises `Ecto.NoResultsError` if the Invite does not exist.

  ## Examples

      iex> get_invite!(123)
      %Invite{}

      iex> get_invite!(456)
      ** (Ecto.NoResultsError)

  """
  def get_invite!(id), do: Repo.get!(Invite, id)

  @doc """
  Creates a invite.

  ## Examples

      iex> create_invite(%{field: value})
      {:ok, %Invite{}}

      iex> create_invite(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_invite(attrs \\ %{}) do
    %Invite{}
    |> Invite.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a invite.

  ## Examples

      iex> update_invite(invite, %{field: new_value})
      {:ok, %Invite{}}

      iex> update_invite(invite, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_invite(%Invite{} = invite, attrs) do
    invite
    |> Invite.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Invite.

  ## Examples

      iex> delete_invite(invite)
      {:ok, %Invite{}}

      iex> delete_invite(invite)
      {:error, %Ecto.Changeset{}}

  """
  def delete_invite(%Invite{} = invite) do
    Repo.delete(invite)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking invite changes.

  ## Examples

      iex> change_invite(invite)
      %Ecto.Changeset{source: %Invite{}}

  """
  def change_invite(%Invite{} = invite) do
    Invite.changeset(invite, %{})
  end
end
