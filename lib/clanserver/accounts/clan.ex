defmodule Clanserver.Accounts.Clan do
  use Ecto.Schema
  import Ecto.Changeset

  schema "clans" do
    field :clantag, :string
    field :name, :string
    field :leader_id, :integer

    has_many(:users, Clanserver.Accounts.User, on_replace: :delete, on_delete: :nothing)

    timestamps()
  end

  @doc false
  def changeset(clan, attrs) do
    clan
    |> cast(attrs, [:name, :clantag, :leader_id])
    |> validate_required([:name, :clantag])
    |> unique_constraint(:name)
    |> unique_constraint(:clantag)
  end
end
