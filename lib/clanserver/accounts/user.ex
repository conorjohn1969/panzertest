defmodule Clanserver.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :nickname, :string
    field :login, :string
    field :password_hash, :string
    field :password, :string, virtual: true
    field :token, :string

    belongs_to :clan, Clanserver.Accounts.Clan
    has_many(:invites, Clanserver.Accounts.Invite, on_replace: :delete, on_delete: :delete_all)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:nickname, :password, :login])
    |> validate_required([:nickname, :password, :login])
    |> unique_constraint(:login)
    |> put_pass_hash
  end

  @doc false
  def clan_changeset(user, attrs) do
    user
    |> cast(attrs, [:clan_id])
    |> validate_required([:clan_id])
  end

  @doc false
  def remove_from_clan_changeset(user, _attrs) do
    attrs =
      %{} |>
        Map.put(:clan_id, nil)
    user
    |> cast(attrs, [:clan_id])
  end

  defp put_pass_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        changeset
        |> put_change(:password_hash, Bcrypt.hash_pwd_salt(pass))
        |> put_change(:token, ClanserverWeb.Authentication.random_token())

      _ ->
        changeset
    end
  end
end
