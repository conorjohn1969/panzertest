defmodule Clanserver.Accounts.Invite do
  use Ecto.Schema
  import Ecto.Changeset

  schema "invites" do
#    field :clan_id, :id
#    field :user_id, :id
    belongs_to :user, Clanserver.Accounts.User
    belongs_to :clan, Clanserver.Accounts.Clan

#    belongs_to(:user, Clanserver.Accounts.User, on_replace: :delete, on_delete: :delete_all)

    timestamps()
  end

  @doc false
  def changeset(invite, attrs) do
    invite
    |> cast(attrs, [:clan_id, :user_id])
    |> validate_required([:clan_id, :user_id])
#    |> cast_assoc(:user)
  end
end
