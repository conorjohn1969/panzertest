defmodule Clanserver.Repo do
  use Ecto.Repo,
    otp_app: :clanserver,
    adapter: Ecto.Adapters.Postgres
end
