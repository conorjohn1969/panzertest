defmodule Clanserver.Repo.Migrations.CreateClans do
  use Ecto.Migration

  def change do
    create table(:clans) do
      add :name, :string
      add :clantag, :string
      add :leader_id, :integer

      timestamps()
    end

    create index(:clans, [:name], unique: true)
    create index(:clans, [:clantag], unique: true)
  end
end
