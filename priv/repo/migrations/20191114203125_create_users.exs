defmodule Clanserver.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :nickname, :string
      add :login, :string
      add :password_hash, :string
      add :token, :string

      timestamps()
    end

    create index(:users, [:login], unique: true)
  end
end
