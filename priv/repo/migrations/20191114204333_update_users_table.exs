defmodule Clanserver.Repo.Migrations.UpdateUsersTable do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add(:clan_id, references(:clans, on_delete: :delete_all))
    end
  end
end
