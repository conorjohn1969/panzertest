defmodule Clanserver.Repo.Migrations.CreateInvites do
  use Ecto.Migration

  def change do
    create table(:invites) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :clan_id, references(:clans, on_delete: :delete_all)

      timestamps()
    end

    create index(:invites, [:user_id])
    create index(:invites, [:clan_id])
  end
end
